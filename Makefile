TAG=rafaelszp/ansible-playbook
docker.build:
	docker build -t ${TAG} . -f Dockerfile

docker.push:
	docker push ${TAG}

docker.test: 
	docker run -it ${TAG} --version
